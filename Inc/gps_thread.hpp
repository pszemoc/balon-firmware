//
// Created by pszemo on 09.12.2019.
//


#pragma once

#include "thread_base.hpp"
#include <cstdint>

namespace radio{
  class radio_thread_t;
}

class gps_thread_t : public thread_base_t<gps_thread_t, 1024> {
private:
  static constexpr char             name[]            = "gps_thread";
  static constexpr osPriority_t     priority          = osPriorityNormal2;
  static inline osMessageQueueId_t  uart_queue        = nullptr;
  static constexpr uint8_t          uart_queue_size   = 8;
  uint8_t                           uart_queue_buffer[uart_queue_size * sizeof(uint8_t)];
  osStaticMessageQDef_t             uart_queue_control_block;

  radio::radio_thread_t*            radio_thread      = nullptr;

public:
  static inline bool    uart_running  = true;
  static inline uint8_t uart_byte     = 0;

  gps_thread_t(radio::radio_thread_t* radio_thread);
  void thread();
  static osMessageQueueId_t get_uart_queue();
  const char* get_name() const;
  const osPriority_t get_priority() const;
};
