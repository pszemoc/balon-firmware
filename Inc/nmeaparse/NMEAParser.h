/*
 * NMEAParser.h
 *
 *  Created on: Aug 12, 2014
 *      Author: Cameron Karlsson
 *
 *  See the license file included with this source.
 */

#ifndef NMEAPARSER_H_
#define NMEAPARSER_H_


#include <nmeaparse/Event.h>
#include <string>
#include <functional>
#include <unordered_map>
#include <vector>
#include <cstdint>
#include <exception>

#include "radio/radio_common.hpp"

//read class definition for info
#define NMEA_PARSER_MAX_BUFFER_SIZE 512

namespace nmea {

  enum class NMEAError {
    NMEAOK,
    ParseError,
    NumberConversionError,
    SentenceEmpty,
    InvalidCharacter
  };

  class NMEAParser;

  class NMEASentence {
    friend NMEAParser;
  private:
    bool isvalid;
  public:
    std::string text;      //whole plaintext of the received command
    std::string name;      //name of the command
    std::vector<std::string> parameters;  //list of parameters from the command
    std::string checksum;
    bool checksumIsCalculated;
    uint8_t parsedChecksum;
    uint8_t calculatedChecksum;

    enum MessageID {    // These ID's are according to NMEA standard.
      Unknown = -1,
      GGA = 0,
      GLL = 1,
      GSA = 2,
      GSV = 3,
      RMC = 4,
      VTG = 5,    // notice missing 6,7
      ZDA = 8
    };
  public:
    NMEASentence();

    virtual ~NMEASentence();

    bool checksumOK() const;

    bool valid() const;

  };

  class NMEAParser {
  private:
    std::unordered_map<std::string, std::function<void(NMEASentence)>> eventTable;
    std::string buffer;
    bool fillingbuffer;
    uint32_t maxbuffersize;    //limit the max size if no newline ever comes... Prevents huge buffer string internally

    NMEAError parseText(NMEASentence &nmea,
                        std::string s);    //fills the given NMEA sentence with the results of parsing the string.

  public:

    NMEAParser();

    virtual ~NMEAParser();

    Event<void(const NMEASentence &)> onSentence;        // called every time parser receives any NMEA sentence
    void setSentenceHandler(std::string cmdKey, std::function<void(
      const NMEASentence &)> handler);  //one handler called for any named sentence where name is the "cmdKey"

    // Byte streaming functions
    NMEAError readByte(uint8_t b);

    void readBuffer(uint8_t *b, uint32_t size);

    void readLine(std::string line);

    // This function expects the data to be a single line with an actual sentence in it, else it throws an error.
    NMEAError readSentence(
      std::string cmd);        // called when parser receives a sentence from the byte stream. Can also be called by user to inject sentences.

    static uint8_t calculateChecksum(std::string);    // returns checksum of string -- XOR
  };

  class GPSTimestamp {
  private:
  public:
    GPSTimestamp();

    int32_t hour;
    int32_t min;
    double sec;

    int32_t month;
    int32_t day;
    int32_t year;

    // Values collected directly from the GPS
    double rawTime;
    int32_t rawDate;

    time_t getTime();

    // Set directly from the NMEA time stamp
    // hhmmss.sss
    void setTime(double raw_ts);

    // Set directly from the NMEA date stamp
    // ddmmyy
    void setDate(int32_t raw_date);

    std::string toString();
  };

struct GPSFix : public radio::tx_data_t{
  float latitude;
  float longitude;
  float altitude;
  int32_t tracking_satellites;
  uint8_t quality;
  GPSTimestamp timestamp;
};

  namespace gpgga{
    constexpr uint8_t timestamp = 0;
    constexpr uint8_t latitude = 1;
    constexpr uint8_t latitude_dir = 2;
    constexpr uint8_t longitude = 3;
    constexpr uint8_t longitude_dir = 4;
    constexpr uint8_t fix_quality = 5;
    constexpr uint8_t tracked_satellites = 6;
    constexpr uint8_t altitude = 8;
  }

  double convertLatLongToDeg(std::string llstr, std::string dir);

}

#endif /* NMEAPARSER_H_ */
