/*
 * NumberConversion.h
 *
 *  Created on: Aug 14, 2014
 *      Author: Cameron Karlsson
 *
 *  See the license file included with this source.
 */

#ifndef NUMBERCONVERSION_H_
#define NUMBERCONVERSION_H_


#include <cstdint>
#include <string>
#include <exception>


namespace nmea {

float parseFloat(std::string s, bool* error = nullptr);
int64_t parseInt(std::string s, int radix = 10, bool* error = nullptr);
}



#endif /* NUMBERCONVERSION_H_ */
