//
// Created by pszemo on 05.12.2019.
//

#pragma once

#include <cstdint>

template <class T>
class mem_ptr_t{
  std::intptr_t addr;
public:
  constexpr mem_ptr_t(std::intptr_t i) : addr{i} {}
  operator T*() const { return reinterpret_cast<T*>(addr); }
  T* operator->() const { return operator T*(); }

  uint8_t* raw() const { return reinterpret_cast<uint8_t*>(addr); }
};


