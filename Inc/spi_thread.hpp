//
// Created by pszemo on 10.12.2019.
//

#pragma once

#include "thread_base.hpp"

namespace radio{
  class radio_thread_t;
}

class spi_thread_t : public thread_base_t<spi_thread_t, 256> {
private:



  static constexpr char             name[]                          = "spi_thread";
  static constexpr osPriority_t     priority                        = osPriorityNormal4;
  static constexpr uint32_t         measurement_interval            = 5000; // [ms]

  static inline osSemaphoreId_t     spi_semaphore                   = nullptr;
  osStaticSemaphore_t               spi_semaphore_control_block;

  radio::radio_thread_t*            radio_thread                    = nullptr;

public:

  spi_thread_t(radio::radio_thread_t *radio_thread);
  void thread();
  const char* get_name() const;
  const osPriority_t get_priority() const;
  static osSemaphoreId_t get_spi_semaphore();
  bool read_temperature(float& temperature);
  bool read_raw_resistance(uint16_t & resistance);
};
