//
// Created by pszemo on 14.12.2019.
//


#pragma once

namespace max31865 {
  constexpr float         adc_resolution          = 32768.0;
  constexpr float         reference_resistor      = 430.0;
  constexpr float         rtd_a                   = 3.9083e-3;
  constexpr float         rtd_b                   = -5.775e-7;
  constexpr float         rtd_nominal             = 100.0;
  constexpr float         z1                      = -rtd_a;
  constexpr float         z2                      = rtd_a*rtd_a - (4*rtd_b);
  constexpr float         z3                      = (4*rtd_b)/rtd_nominal;
  constexpr float         z4                      = 2*rtd_b;
}

