#pragma once
#include <string>
#include <cstdint>
#include "stm32l4xx_hal.h"
#include "cmsis_os2.h"

namespace radio {
  namespace cmd {
    namespace sys {
      constexpr char get_ver[]        = "sys get ver\r\n";
      constexpr char sleep[]          = "sys sleep ";
    }
    namespace mac {
      constexpr char pause[]          = "mac pause\r\n";
    }
    namespace radio {
      constexpr char rx[]             = "radio rx ";
      constexpr char tx[]             = "radio tx ";
      constexpr char set_mod[]        = "radio set mod ";
      constexpr char set_freq[]       = "radio set freq ";
      constexpr char set_pwr[]        = "radio set pwr ";
      constexpr char set_sf[]         = "radio set sf ";
      constexpr char set_bw[]         = "radio set bw ";
      constexpr char set_cr[]         = "radio set cr ";
      constexpr char set_iq[]         = "radio set iqi ";
      constexpr char set_sync[]       = "radio set sync ";
      constexpr char set_wdt[]        = "radio set wdt ";

      constexpr char set_bt[]         = "radio set bt ";
      constexpr char set_afcbw[]      = "radio set afcbw ";
      constexpr char set_rxbw[]       = "radio set rxbw ";
      constexpr char set_bitrate[]    = "radio set bitrate ";
      constexpr char set_fdev[]       = "radio set fdev ";
      constexpr char set_prlen[]      = "radio set prlen ";
      constexpr char set_crc[]        = "radio set crc ";


      constexpr char get_snr[]        = "radio get snr\r\n";
      namespace options {
        constexpr char lora[]       = "lora";
        constexpr char sf7[]        = "sf7";
        constexpr char sf8[]        = "sf8";
        constexpr char sf9[]        = "sf9";
        constexpr char sf10[]       = "sf10";
        constexpr char sf11[]       = "sf11";
        constexpr char sf12[]       = "sf12";
        constexpr char on[]         = "on";
        constexpr char off[]        = "off";
        constexpr char cr4_5[]      = "4/5";
        constexpr char cr4_6[]      = "4/6";
        constexpr char cr4_7[]      = "4/7";
        constexpr char cr4_8[]      = "4/8";
      }
    }
    namespace response {
      constexpr char ok[]             = "ok\r\n";
      constexpr char invalid_param[]  = "invalid_param\r\n";
      constexpr char busy[]           = "busy\r\n";
      constexpr char radio_rx[]       = "radio_rx";
      constexpr char radio_tx_ok[]    = "radio_tx_ok\r\n";
      constexpr char radio_err[]      = "radio_err\r\n";
    }
  }

  enum class radio_error : uint8_t {
    ok,
    timeout,
    tx_invalid_param,
    tx_error,
    config_error
  };

  class radio_controller_t {
  private:
    UART_HandleTypeDef* huart;
    osMessageQueueId_t  uart_queue;

    uint64_t pause_time;

    std::string to_hex(const uint8_t* buffer, uint32_t size);
    radio_error config_lora();
    radio_error config_fsk();

  public:
    static inline uint8_t uart_byte = 0;

    radio_controller_t(UART_HandleTypeDef *huart, osMessageQueueId_t uart_queue);
    ~radio_controller_t();

    void restart();
    radio_error config();
    radio_error send_cmd(const char* cmd);
    radio_error send_cmd(const char* cmd, const char* arg);
    radio_error get_response(std::string& str);
    radio_error send_and_receive_cmd(const char *cmd, std::string &buffer);
    radio_error send_and_receive_cmd(const char *cmd, const char *arg, std::string &buffer);
    radio_error transmit(const uint8_t* buffer, uint32_t size);
  };
}
