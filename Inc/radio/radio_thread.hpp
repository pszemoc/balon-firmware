//
// Created by pszemo on 09.12.2019.
//

#pragma once

#include "thread_base.hpp"
#include "radio/radio_common.hpp"
#include "telemetry.hpp"
#include <vector>

namespace radio {
  class radio_thread_t : public thread_base_t<radio_thread_t, 256> {
  private:
    static constexpr uint32_t         tx_interval       = 12000; // [ms]
    uint32_t                          last_tx_time      = 0;

    static constexpr char             name[]            = "radio_thread";
    static constexpr osPriority_t     priority          = osPriorityNormal1;

    osMessageQueueId_t                tx_queue          = nullptr;
    static constexpr uint8_t          tx_queue_size     = 8;
    uint8_t                           tx_queue_buffer[tx_queue_size * sizeof(tx_msg_t)];
    osStaticMessageQDef_t             tx_queue_control_block;

    static inline osTimerId_t         led_off_timer     = nullptr;
    osStaticTimerDef_t                led_off_timer_control_block;
    static constexpr uint32_t         led_off_delay     = 100;

    static inline osMessageQueueId_t  uart_queue        = nullptr;
    static constexpr uint8_t          uart_queue_size   = 8;
    uint8_t                           uart_queue_buffer[uart_queue_size * sizeof(uint8_t)];
    osStaticMessageQDef_t             uart_queue_control_block;

    std::vector<radio::tx_msg_t>      tx_vector;
    telemetry::packet_manager_t       packet_manager;

  public:
    radio_thread_t();
    void thread();
    osMessageQueueId_t get_tx_queue() const ;
    static osMessageQueueId_t get_uart_queue();
    const char * get_name() const;
    const osPriority_t get_priority() const;
    static void led_off(void* arg);
  };
}
