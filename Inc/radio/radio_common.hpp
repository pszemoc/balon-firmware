//
// Created by pszemo on 02.12.2019.
//

#ifndef STRATOSAT_FIRMWARE_RADIO_COMMON_HPP
#define STRATOSAT_FIRMWARE_RADIO_COMMON_HPP

namespace radio {
  namespace setup{
    constexpr char    mode[]      = "fsk";
    constexpr char    freq[]      = "434750000";
    constexpr char    pwr[]       = "15";
    constexpr char    wdt[]       = "0";
    // lora
    constexpr char    sf[]        = "sf7";
    constexpr char    bw[]        = "125";
    constexpr char    cr[]        = "4/5";
    constexpr char    sync[]      = "12";
    constexpr char    iq[]        = "off";
    // fsk
    constexpr char    acfcb[]     = "2.6";
    constexpr char    rxbw[]      = "2.6";
    constexpr char    bitrate[]   = "300";
    constexpr char    fdev[]      = "75";
    constexpr char    prlen[]     = "8";
    constexpr char    bt[]        = "0.5";
    constexpr char    crc[]       = "off";
  }

  enum class tx_type : uint32_t {
    gps,
    pressure,
    ambient_temperature,
    internal_temperature,
    mcu_temperature,
    humidity,
    muons,
  };

  struct tx_data_t {
    virtual ~tx_data_t() {}
  };

  struct single_measurement_t : public tx_data_t {
    float val;
    single_measurement_t(float val) : val(val) {}
  };

  struct tx_msg_t {
    uint32_t        timestamp;
    tx_type         type;
    tx_data_t*      data;
  };
}

#endif //STRATOSAT_FIRMWARE_RADIO_COMMON_HPP
