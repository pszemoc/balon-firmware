//
// Created by pszemo on 10.12.2019.
//

#include "spi_thread.hpp"
#include "peripherals.hpp"
#include <cmath>
#include "radio/radio_thread.hpp"
#include "radio/radio_common.hpp"
#include <algorithm>
#include "max31865.hpp"

spi_thread_t::spi_thread_t(radio::radio_thread_t *radio_thread)
: radio_thread(radio_thread) {
  const osSemaphoreAttr_t sem_attr = {
    .name = "spi sem",
    .attr_bits = 0,
    .cb_mem = &spi_semaphore_control_block,
    .cb_size = sizeof(spi_semaphore_control_block)
  };
  spi_semaphore = osSemaphoreNew(1, 0, &sem_attr);
}

void spi_thread_t::thread() {
  uint32_t measurement_start;
  uint32_t measurement_end;
  float temperature;

  for (;;) {
    measurement_start = osKernelGetTickCount();
    if (read_temperature(temperature))  {
      radio::single_measurement_t* measurement = new radio::single_measurement_t{temperature};
      radio::tx_msg_t msg{osKernelGetTickCount(), radio::tx_type::ambient_temperature, measurement};
      osMessageQueuePut(radio_thread->get_tx_queue(), &msg, 0, 1000);
    }
    measurement_end = osKernelGetTickCount();
    osDelay(measurement_interval - (measurement_end - measurement_start));
  }
}

const char* spi_thread_t::get_name() const {
  return name;
}

const osPriority_t spi_thread_t::get_priority() const {
  return priority;
}

osSemaphoreId_t spi_thread_t::get_spi_semaphore() {
  return spi_semaphore;
}

bool spi_thread_t::read_temperature(float& temperature) {
  uint16_t raw_resistance;
  if(read_raw_resistance(raw_resistance)){
    //https://www.analog.com/media/en/technical-documentation/application-notes/AN709_0.pdf
    float resistance = static_cast<float>(raw_resistance) / max31865::adc_resolution * max31865::reference_resistor;
    temperature = max31865::z2 + (max31865::z3 * resistance);
    temperature = (std::sqrt(temperature) + max31865::z1) + max31865::z4;
    if (temperature >= 0 ){
      return true;
    }
    float rpoly = resistance;
    temperature = -242.02;
    temperature += 2.2228 * rpoly;
    rpoly *= resistance;
    temperature += 2.5859e-3 * rpoly;
    rpoly *= resistance;
    temperature -= 4.8260e-6 * rpoly;
    rpoly *= resistance;
    temperature -= 2.8183e-8 * rpoly;
    rpoly *= resistance;
    temperature += 1.5243e-10 * rpoly;
    return true;
  } else {
    return false;
  }
}

bool spi_thread_t::read_raw_resistance(uint16_t &resistance) {
  osStatus_t status;
  uint8_t tx_buf[3];
  uint8_t rx_buf[3];


  tx_buf[0] = 0x80;
  tx_buf[1] = 0b10010001; // vbias on, 3 wire, 50 hz
  HAL_GPIO_WritePin(peripherals::pt100_cs_port, peripherals::pt100_cs_pin, GPIO_PIN_RESET);
  HAL_SPI_TransmitReceive_IT(&peripherals::hspi1, tx_buf, rx_buf, 2);
  status = osSemaphoreAcquire(spi_semaphore, 1000);
  if (status != osOK) {
    return false;
  }

  osDelay(20);
  tx_buf[0] = 0x80;
  tx_buf[1] = 0b10110001; // vbias on, 1-shot, 3 wire, 50 hz
  HAL_GPIO_WritePin(peripherals::pt100_cs_port, peripherals::pt100_cs_pin, GPIO_PIN_RESET);
  HAL_SPI_TransmitReceive_IT(&peripherals::hspi1, tx_buf, rx_buf, 2);
  status = osSemaphoreAcquire(spi_semaphore, 1000);
  if (status != osOK) {
    return false;
  }

  osDelay(100);
  tx_buf[0] = 0x01;
  tx_buf[1] = 0x00;
  HAL_GPIO_WritePin(peripherals::pt100_cs_port, peripherals::pt100_cs_pin, GPIO_PIN_RESET);
  HAL_SPI_TransmitReceive_IT(&peripherals::hspi1, tx_buf, rx_buf, 3);
  status = osSemaphoreAcquire(spi_semaphore, 1000);
  if (status != osOK) {
    return false;
  }

  resistance = ( rx_buf[1] << 8 | rx_buf[2] ) >> 1;
  return true;
}
