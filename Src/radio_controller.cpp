#include "radio/radio_controller.hpp"
#include "peripherals.hpp"
#include <sstream>
#include <iomanip>
#include <radio/radio_common.hpp>

using namespace radio;

radio_controller_t::radio_controller_t(UART_HandleTypeDef *huart, osMessageQueueId_t uart_queue)
: huart(huart)
, uart_queue(uart_queue)
{}

radio_controller_t::~radio_controller_t() {}

radio_error radio_controller_t::config_lora() {
  std::string res;
  radio_error err = radio_error::ok;
  err = send_and_receive_cmd(cmd::radio::set_sf, setup::sf, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_bw, setup::bw, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_cr, setup::cr, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_sync, setup::sync, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_iq, setup::iq, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }
  return radio_error::ok;
}

radio_error radio_controller_t::config_fsk() {
  std::string res;
  radio_error err = radio_error::ok;
  err = send_and_receive_cmd(cmd::radio::set_bt, setup::bt, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_afcbw, setup::acfcb, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_rxbw, setup::rxbw, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_bitrate, setup::bitrate, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_fdev, setup::fdev, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_prlen, setup::prlen, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_crc, setup::crc, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }
  return radio_error::ok;
}

constexpr bool strings_equal(char const * a, char const * b) {
  return *a == *b && (*a == '\0' || strings_equal(a + 1, b + 1));
}

radio_error radio_controller_t::config() {
  std::string res;
  radio_error err = send_and_receive_cmd(cmd::mac::pause, res);
  if (err != radio_error::ok || res.size() == 0) {
    return radio_error::config_error;
  }
  // TODO: save pause time

  err = send_and_receive_cmd(cmd::radio::set_mod, setup::mode, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_freq, setup::freq, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  err = send_and_receive_cmd(cmd::radio::set_pwr, setup::pwr, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }

  if constexpr (strings_equal(setup::mode, cmd::radio::options::lora)) {
    config_lora();
  } else {
    config_fsk();
  }

  err = send_and_receive_cmd(cmd::radio::set_wdt, setup::wdt, res);
  if (err != radio_error::ok && res != std::string(cmd::response::ok)) {
    return radio_error::config_error;
  }
  return radio_error::ok;
}

radio_error radio_controller_t::send_cmd(const char* cmd) {
  std::string buffer(cmd);
  HAL_UART_Transmit_IT(huart, reinterpret_cast<uint8_t*>(buffer.data()), buffer.size());
  if (osMessageQueueGet(uart_queue, &uart_byte, nullptr, 1000) == osOK) {
    return radio_error::ok;
  }
  return radio_error::timeout;
}

radio_error radio_controller_t::send_cmd(const char* cmd, const char* arg) {
  std::string buffer(cmd);
  buffer += std::string(arg);
  buffer += "\r\n";
  HAL_UART_Transmit_IT(huart, reinterpret_cast<uint8_t*>(buffer.data()), buffer.size());
  if (osMessageQueueGet(uart_queue, &uart_byte, nullptr, 1000) == osOK) {
    return radio_error::ok;
  }
  return radio_error::timeout;
}

radio_error radio_controller_t::get_response(std::string& str) {
  uint8_t c;
  str.clear();
  HAL_UART_Receive_IT(huart, &uart_byte, 1);
  while (osMessageQueueGet(uart_queue, &c, nullptr, 3000) == osOK) {
    str.push_back(c);
    if(str.find("\r\n") != std::string::npos) {
      HAL_UART_AbortReceive_IT(huart);
      return radio_error::ok;
    }
  }
  return radio_error::timeout;
}

radio_error radio_controller_t::send_and_receive_cmd(const char *cmd, std::string &buffer) {
  radio_error err = send_cmd(cmd);
  if (err == radio_error::ok) {
    return get_response(buffer);
  } else {
    return err;
  }
}

radio_error radio_controller_t::send_and_receive_cmd(const char *cmd, const char *arg, std::string &buffer) {
  radio_error err = send_cmd(cmd, arg);
  if (err == radio_error::ok) {
    return get_response(buffer);
  } else {
    return err;
  }
}

std::string radio_controller_t::to_hex(const uint8_t* buffer, uint32_t size) {
  std::stringstream ss;
  for (unsigned int i = 0; i < size; ++i) {
    ss << std::setfill('0') << std::setw(2) << std::hex << (unsigned int)buffer[i];
  }
  return ss.str();
}

radio_error radio_controller_t::transmit(const uint8_t* buffer, uint32_t size) {
  std::string hex = to_hex(buffer, size);
  std::string res;
  radio_error err = send_and_receive_cmd(cmd::radio::tx, hex.c_str(), res);
  if (err != radio_error::ok) {
    return err;
  }
  if (res != std::string(cmd::response::ok)) {
    return radio_error::tx_invalid_param;
  }
  err = get_response(res);
  if (err != radio_error::ok) {
    return err;
  }
  if (res != std::string(cmd::response::radio_tx_ok)){
    return radio_error::tx_error;
  }
  return radio_error::ok;
}

void radio_controller_t::restart() {
  std::string buffer;
  radio_error err = radio_error::timeout;
  while(err == radio_error::timeout) {
    HAL_GPIO_WritePin(peripherals::radio_rst_port, peripherals::radio_rst_pin, GPIO_PIN_RESET);
    osDelay(100);
    HAL_GPIO_WritePin(peripherals::radio_rst_port, peripherals::radio_rst_pin, GPIO_PIN_SET);
    err = get_response(buffer);
  }
}


