//
// Created by pszemo on 09.12.2019.
//

#include "radio/radio_thread.hpp"
#include "peripherals.hpp"
#include "radio/radio_controller.hpp"
#include <algorithm>

radio::radio_thread_t::radio_thread_t() {
  const osMessageQueueAttr_t sensor_queue_attr = {
    .name = "tx_queue",
    .attr_bits = 0,
    .cb_mem = &tx_queue_control_block,
    .cb_size = sizeof(tx_queue_control_block),
    .mq_mem = &tx_queue_buffer,
    .mq_size = sizeof(tx_queue_buffer)
  };
  tx_queue = osMessageQueueNew(tx_queue_size, sizeof(tx_msg_t), &sensor_queue_attr);

  const osMessageQueueAttr_t uart_queue_attr = {
    .name = "uart_queue",
    .attr_bits = 0,
    .cb_mem = &uart_queue_control_block,
    .cb_size = sizeof(uart_queue_control_block),
    .mq_mem = &uart_queue_buffer,
    .mq_size = sizeof(uart_queue_buffer)
  };
  uart_queue = osMessageQueueNew(uart_queue_size, sizeof(uint8_t), &uart_queue_attr);

  const osTimerAttr_t led_off_timer_attributes = {
    .name = "led_off_timer_",
    .attr_bits = 0,
    .cb_mem = &led_off_timer_control_block,
    .cb_size = sizeof(led_off_timer_control_block)
  };
  led_off_timer = osTimerNew(led_off, osTimerOnce, nullptr, &led_off_timer_attributes);
}

void radio::radio_thread_t::thread() {
  osStatus_t status;
  radio::tx_msg_t msg;
  radio_controller_t radio(&peripherals::huart2, uart_queue);
  radio.restart();
  while(radio.config() != radio_error::ok) {
    radio.restart();
  }

  for (;;) {
    status = osMessageQueueGet(tx_queue, &msg, nullptr, 1);
    if (status == osOK) {
      auto it = std::find_if(tx_vector.begin(), tx_vector.end(), [&msg] (tx_msg_t& msg_in_vector) {
        return msg.type == msg_in_vector.type;
      });
      if (it != tx_vector.end()) {
        it->timestamp = msg.timestamp;
        delete it->data;
        it->data = msg.data;
      } else {
        tx_vector.push_back(msg);
      }

      HAL_GPIO_WritePin(peripherals::led_port, peripherals::led_pin, GPIO_PIN_RESET);
      if (led_off_timer != nullptr && !osTimerIsRunning(led_off_timer)) {
        osTimerStart(led_off_timer, led_off_delay);
      }
    }
    if (osKernelGetTickCount() - last_tx_time > tx_interval) {
      packet_manager.encode_packet(tx_vector, osKernelGetTickCount(), 0);
      while(tx_vector.size()) {
        delete tx_vector.back().data;
        tx_vector.pop_back();
      }

      last_tx_time = osKernelGetTickCount();
      radio.transmit(packet_manager.get_buffer(), telemetry::tm_frame_length);
    }
  }
}

osMessageQueueId_t radio::radio_thread_t::get_tx_queue() const {
  return tx_queue;
}

const char * radio::radio_thread_t::get_name() const {
  return name;
}

const osPriority_t radio::radio_thread_t::get_priority() const {
  return priority;
}

void radio::radio_thread_t::led_off(void *arg) {
  HAL_GPIO_WritePin(peripherals::led_port, peripherals::led_pin, GPIO_PIN_SET);
}

osMessageQueueId_t radio::radio_thread_t::get_uart_queue() {
  return uart_queue;
}
