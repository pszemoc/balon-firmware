/*
 * NumberConversion.cpp
 *
 *  Created on: Sep 2, 2014
 *      Author: Cameron Karlsson
 *
 *  See the license file included with this source.
 */

#include <nmeaparse/NumberConversion.h>
#include <cstdlib>

using namespace std;

namespace nmea {
// Note: both parseFloat and parseInt return 0 with "" input.

		float parseFloat(std::string s, bool* error){

			char* p;
			float d = ::strtof(s.c_str(), &p);
			if (*p != 0){
				if (error != nullptr) {
          *error = true;
				}
			}
			return d;
		}
		int64_t parseInt(std::string s, int radix, bool* error){
			char* p;

			int64_t d = ::strtoll(s.c_str(), &p, radix);

			if (*p != 0) {
        if (error != nullptr) {
          *error = true;
        }
			}
			return d;
		}
}
