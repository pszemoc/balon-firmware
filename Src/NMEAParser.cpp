/*
 * NMEAParser.cpp
 *
 *  Created on: Aug 12, 2014
 *      Author: Cameron Karlsson
 *
 *  See the license file included with this source.
 */

#include <nmeaparse/NMEAParser.h>
#include <nmeaparse/NumberConversion.h>
#include <algorithm>
#include <cctype>
#include <ctime>
#include <cmath>
#include <sstream>

using namespace std;
using namespace nmea;


// --------- NMEA SENTENCE --------------

NMEASentence::NMEASentence() 
: isvalid(false)
, checksumIsCalculated(false)
, calculatedChecksum(0)
, parsedChecksum(0)
{ }

NMEASentence::~NMEASentence()
{ }

bool NMEASentence::valid() const {
	return isvalid;
}

bool NMEASentence::checksumOK() const {
	return (checksumIsCalculated)
		&&
		(parsedChecksum == calculatedChecksum);
}

// true if the text contains a non-alpha numeric value
bool hasNonAlphaNum(string txt){
	for (const char i : txt){
		if ( !isalnum(i) ){
			return true;
		}
	}
	return false;
}

// true if alphanumeric or '-'
bool validParamChars(string txt){
	for (const char i : txt){
		if (!isalnum(i)){
			if (i != '-' && i != '.'){
				return false;
			}
		}
	}
	return true;
}

// remove all whitespace
void squish(string& str){

	char chars[] = {'\t',' '};
	for (const char i : chars)
	{
		// needs include <algorithm>
		str.erase(std::remove(str.begin(), str.end(), i), str.end());
	}
}

// remove side whitespace
void trim(string& str){
	stringstream trimmer;
	trimmer << str;
	str.clear();
	trimmer >> str;
}


// --------- NMEA PARSER --------------



NMEAParser::NMEAParser() 
: maxbuffersize(NMEA_PARSER_MAX_BUFFER_SIZE)
, fillingbuffer(false)
{ }

NMEAParser::~NMEAParser() 
{ }


void NMEAParser::setSentenceHandler(std::string cmdKey, std::function<void(const NMEASentence&)> handler){
	eventTable.erase(cmdKey);
	eventTable.insert({ cmdKey, handler });
}

NMEAError NMEAParser::readByte(uint8_t b){
	uint8_t startbyte = '$';

	if (fillingbuffer){
		if (b == '\n'){
			buffer.push_back(b);
			NMEAError err = readSentence(buffer);
			buffer.clear();
			fillingbuffer = false;
			if (err != NMEAError::NMEAOK) {
			  return err;
			}
		}
		else{
			if (buffer.size() < maxbuffersize){
				buffer.push_back(b);
			}
			else {
				buffer.clear();			//clear the host buffer so it won't overflow.
				fillingbuffer = false;
			}
		}
	}
	else {
		if (b == startbyte){			// only start filling when we see the start byte.
			fillingbuffer = true;
			buffer.push_back(b);
		}
	}
	return NMEAError::NMEAOK;
}

void NMEAParser::readBuffer(uint8_t* b, uint32_t size){
	for (uint32_t i = 0; i < size; ++i){
		readByte(b[i]);
	}
}

void NMEAParser::readLine(string cmd){
	cmd += "\r\n";
	for (const char i : cmd){
		readByte(i);
	}
}

// takes a complete NMEA string and gets the data bits from it,
// calls the corresponding handler in eventTable, based on the 5 letter sentence code
NMEAError NMEAParser::readSentence(std::string cmd){

	NMEASentence nmea;

	if (cmd.empty()){
		return NMEAError::SentenceEmpty;
	}
	
	// If there is a newline at the end (we are coming from the byte reader
	if ( *(cmd.end()-1) == '\n'){
		if (*(cmd.end() - 2) == '\r'){	// if there is a \r before the newline, remove it.
			cmd = cmd.substr(0, cmd.size() - 2);
		}
		else
		{
			cmd = cmd.substr(0, cmd.size()-1);
		}
	}


	// Remove all whitespace characters.
	size_t beginsize = cmd.size();
	squish(cmd);

	// Seperates the data now that everything is formatted
	if (parseText(nmea, cmd) != NMEAError::NMEAOK) {
	  return NMEAError::ParseError;
	}

	// Handle/Throw parse errors
	if (!nmea.valid()){
		return NMEAError::ParseError;
	}

	// Call the "any sentence" event handler, even if invalid checksum, for possible logging elsewhere.
	onSentence(nmea);

	// Call event handlers based on map entries
	function<void(const NMEASentence&)> handler = eventTable[nmea.name];
	if (handler){
		handler(nmea);
	}
	return NMEAError::NMEAOK;
}

// takes the string *between* the '$' and '*' in nmea sentence,
// then calculates a rolling XOR on the bytes
uint8_t NMEAParser::calculateChecksum(string s){
	uint8_t checksum = 0;
	for (const char i : s){
		checksum = checksum ^ i;
	}
	return checksum;
}


NMEAError NMEAParser::parseText(NMEASentence& nmea, string txt){

	if (txt.empty()){
		nmea.isvalid = false;
		return NMEAError::SentenceEmpty;
	}

	nmea.isvalid = false;	// assume it's invalid first
	nmea.text = txt;		// save the received text of the sentence

	// Looking for index of last '$'
	size_t startbyte = 0;
	size_t dollar = txt.find_last_of('$');
	if (dollar == string::npos){
		// No dollar sign... INVALID!
		return NMEAError::ParseError;
	}
	else
	{
		startbyte = dollar;
	}

	// Get rid of data up to last'$'
	txt = txt.substr(startbyte + 1);

	// Look for checksum
	size_t checkstri = txt.find_last_of('*');
	bool haschecksum = checkstri != string::npos;
	if (haschecksum){
		// A checksum was passed in the message, so calculate what we expect to see
		nmea.calculatedChecksum = calculateChecksum(txt.substr(0, checkstri));
	}
	else
	{
		// No checksum is only a warning because some devices allow sending data with no checksum.
	}

	// Handle comma edge cases
	size_t comma = txt.find(',');
	if (comma == string::npos){		//comma not found, but there is a name...
		if (!txt.empty())
		{	// the received data must just be the name
			if ( hasNonAlphaNum(txt) ){
				nmea.isvalid = false;
				return NMEAError::ParseError;
			}
			nmea.name = txt;
			nmea.isvalid = true;
			return NMEAError::NMEAOK;
		}
		else
		{	//it is a '$' with no information
			nmea.isvalid = false;
			return NMEAError::ParseError;
		}
	}

	//"$," case - no name
	if (comma == 0){
		nmea.isvalid = false;
		return NMEAError::ParseError;
	}

	//name should not include first comma
	nmea.name = txt.substr(0, comma);	
	if ( hasNonAlphaNum(nmea.name) ){
		nmea.isvalid = false;
		return NMEAError::ParseError;
	}


	//comma is the last character/only comma
	if (comma + 1 == txt.size()){		
		nmea.parameters.push_back("");
		nmea.isvalid = true;
		return NMEAError::NMEAOK;
	}


	//move to data after first comma
	txt = txt.substr(comma + 1, txt.size() - (comma + 1));

	//parse parameters according to csv
	istringstream f(txt);
	string s;
	while (getline(f, s, ',')) {
		nmea.parameters.push_back(s);
	}


	//above line parsing does not add a blank parameter if there is a comma at the end...
	// so do it here.
	if (*(txt.end() - 1) == ','){

		// supposed to have checksum but there is a comma at the end... invalid
		if (haschecksum){
			nmea.isvalid = false;
			return NMEAError::ParseError;
		}

		nmea.parameters.push_back("");
	}
	else
	{
		//possible checksum at end...
		size_t endi = nmea.parameters.size() - 1;
		size_t checki = nmea.parameters[endi].find_last_of('*');
		if (checki != string::npos){
			string last = nmea.parameters[endi];
			nmea.parameters[endi] = last.substr(0, checki);
			if (checki == last.size() - 1){
			  return NMEAError::ParseError;
			}
			else{
				nmea.checksum = last.substr(checki + 1, last.size() - checki);		//extract checksum without '*'
			  bool error = false;
				nmea.parsedChecksum = (uint8_t)parseInt(nmea.checksum, 16, &error);
			  if(error) {
          return NMEAError::ParseError;
			  }
				nmea.checksumIsCalculated = true;
			}
		}
	}

	for (size_t i = 0; i < nmea.parameters.size(); i++){
		if (!validParamChars(nmea.parameters[i])){
			nmea.isvalid = false;
			return NMEAError::InvalidCharacter;
		}
	}

	nmea.isvalid = true;

	return NMEAError::NMEAOK;
}

GPSTimestamp::GPSTimestamp(){
  hour = 0;
  min = 0;
  sec = 0;

  month = 1;
  day = 1;
  year = 1970;

  rawTime = 0;
  rawDate = 0;
};

// Returns seconds since Jan 1, 1970. Classic Epoch time.
time_t GPSTimestamp::getTime() {
  struct tm t = { 0 };
  t.tm_year = year - 1900;	// This is year-1900, so 112 = 2012
  t.tm_mon = month;			// month from 0:Jan
  t.tm_mday = day;
  t.tm_hour = hour;
  t.tm_min = min;
  t.tm_sec = (int)sec;
  return mktime(&t);
}

void GPSTimestamp::setTime(double raw_ts){
  rawTime = raw_ts;

  hour = (int32_t)std::trunc(raw_ts / 10000.0);
  min = (int32_t)std::trunc((raw_ts - hour * 10000) / 100.0);
  sec = raw_ts - min * 100 - hour * 10000;
}

//ddmmyy
void GPSTimestamp::setDate(int32_t raw_date){
  rawDate = raw_date;
  // If uninitialized, use posix time.
  if(rawDate == 0) {
    month = 1;
    day = 1;
    year = 1970;
  }
  else {
    day = (int32_t)std::trunc(raw_date / 10000.0);
    month = (int32_t)std::trunc((raw_date - 10000 * day) / 100.0);
    year = raw_date - 10000 * day - 100 * month + 2000;
  }
}

std::string GPSTimestamp::toString(){
  std::stringstream ss;
  ss << day << "-" << month << "-" << year << " " << hour << ":" << min << ":" << sec;
  return ss.str();
}

double nmea::convertLatLongToDeg(string llstr, string dir){

  double pd = parseFloat(llstr);
  double deg = trunc(pd / 100);				//get ddd from dddmm.mmmm
  double mins = pd - deg * 100;

  deg = deg + mins / 60.0;

  char hdg = 'x';
  if (!dir.empty()){
    hdg = dir[0];
  }

  //everything should be N/E, so flip S,W
  if (hdg == 'S' || hdg == 'W'){
    deg *= -1.0;
  }

  return deg;
}

